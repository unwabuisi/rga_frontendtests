// PROCESS ===================================================


// STEPS:
// 1. set up the board
    // - assign each number/element an #ID_tag
    // - create table populated with html elements

// 2. Add click event listeners
// 3. Be able to find blank tile and adjacent tiles with every click
// 4. When a tile clicked, if the blank tile is adjacent - SWITCH the two
// 5. BONUS: add shuffle feature


// FUNCTIONS ================================================

// adds the appropriate #ID_tag to each tile element in DOM
var setID = function() {
    var tile = document.getElementsByClassName('tile');
    for (var i = 0; i < tile.length; i++) {
        tile[i].setAttribute('id',tile[i].innerText);
    }

    //creates a blank tile and appends it to the list with #ID_tag '9'
    var blankTile = document.createElement('li');
    blankTile.setAttribute('class','tile');
    blankTile.setAttribute('id','9');
    blankTile.innerHTML = " ";
    var last = document.getElementsByClassName('puzzle');
    last[0].appendChild(blankTile);
};

// creates table and updates DOM
var createTable = function() {
    var table = document.createElement('table');
    table.border = "15";
    table.setAttribute('style','background-color:black;');
    table.setAttribute('class', 'puzzle');

    var tiles = document.getElementsByClassName('tile');

    var counter = 0;
    for (var i = 0; i < 3 ; i++) {
        var row = table.insertRow();

        for (var j = 0; j < 3; j++) {
            var cell = row.insertCell();
            cell.innerHTML = tiles[counter].outerHTML; //outerHTML was used to preserve the class and ID attributes
            cell.setAttribute('data-position', i + '-' + j );
            counter++;
        }
    }

    var puzzle = document.getElementsByClassName('puzzle');

    for (var k = 0; k < puzzle.length; k++) {
        puzzle[k].outerHTML = table.outerHTML;
    }

};

// creates table that is shuffled and updates DOM
var shuffleTable = function() {
    var table = document.createElement('table');
    table.border = "15";
    table.setAttribute('style','background-color:black;');
    table.setAttribute('class', 'puzzle');

    var tiles = document.getElementsByClassName('tile');
    var shufArr = shuffleArray();


    var counter = 0;
    for (var i = 0; i < 3 ; i++) {
        var row = table.insertRow();

        for (var j = 0; j < 3; j++) {
            var cell = row.insertCell();
            console.log();
            cell.innerHTML = tiles[shufArr[counter]-1].outerHTML; //outerHTML was used to preserve the class and ID attributes
            cell.setAttribute('data-position', i + '-' + j );
            counter++;
        }
    }

    var puzzle = document.getElementsByClassName('puzzle');

    for (var k = 0; k < puzzle.length; k++) {
        puzzle[k].outerHTML = table.outerHTML;
    }

};

function shuffleArray(){
    var newArr = [1,2,3,4,5,6,7,8,9];

    var a, b;
    for (var i = newArr.length-1; i > 0; i--) {
        a = Math.floor(Math.random() * (i+1));
        b = newArr[i];
        newArr[i] = newArr[a];
        newArr[a] = b;
    }
    return newArr;
}

// returns cells position or HTML element based on input/output
function cellSearch(cell, whatToFind) {

    var puzzle = document.querySelector('.puzzle').children;
    var rows = puzzle[0].children;
    var adjCells = [];
    var blankTile = [];


    // this loop traverses the each cell/tile in the table, storing both the position
    // as well as the tile within each cell
    for (var i = 0; i < rows.length; i++) {
        for (var j = 0; j < rows[i].children.length; j++) {
            var currentCell = rows[i].children[j];
            var tile = currentCell.children[0];

            switch (whatToFind) {
                case 'blank': // looks for & returns array with position of the blank tile
                    if (tile.id == '9') {
                        var t = currentCell.getAttribute('data-position').split('-');
                        blankTile = t.map(function(x) {
                            return parseInt(x);
                        });
                    return blankTile;
                    }
                    break;

                case 'adj': // returns array of adjacent cell positions
                    if (tile == cell) {
                        var pos = currentCell.getAttribute('data-position').split('-');
                        var tile_position = pos.map(function(x) {
                            return parseInt(x);
                        });

                        //searches by column and pushes adjacent cells to array
                        if (tile_position[1] == 0) {
                            adjCells.push([tile_position[0], tile_position[1]+1]);
                        }
                        else if (tile_position[1] == 1) {
                            adjCells.push([tile_position[0], tile_position[1] + 1]);
                            adjCells.push([tile_position[0], tile_position[1] - 1]);
                        }
                        else if (tile_position[1] == 2) {
                            adjCells.push([tile_position[0], tile_position[1] - 1]);
                        }

                        //searches by rows and pushes adjacent cells to array
                        if (tile_position[0] == 0) {
                            adjCells.push([tile_position[0]+1, tile_position[1]]);
                        }
                        else if (tile_position[0] == 1) {
                            adjCells.push([tile_position[0]-1,tile_position[1]]);
                            adjCells.push([tile_position[0]+1, tile_position[1]]);
                        }
                        else if (tile_position[0] == 2) {
                            adjCells.push([tile_position[0]-1, tile_position[1]]);
                        }
                    return adjCells;
                    }

                    break;
                case 'pos': // returns array with position data for cell
                    if (tile == cell) {
                        var pos = currentCell.getAttribute('data-position').split('-');
                        var position = pos.map(function(x) {
                            return parseInt(x);
                        });
                        return position;
                    }
                    break;
                case 'elem': //takes in position array and returns cell HTML element
                    var elem = currentCell.getAttribute('data-position').split('-');
                    var current_elem_pos = elem.map(function(x) {
                        return parseInt(x);
                    });

                    if (Array.isArray(cell)) {
                        if (cell[0] == current_elem_pos[0] && cell[1] == current_elem_pos[1]) {
                            return currentCell;
                        }
                    }


                    break;
            }
        }
    }
}

// checks to make sure the clicked cell is adjacent to a blank cell before trying to swap
function checkSwap(blankTile,adjacentCells,clickedCell) {
    for (var i = 0; i < adjacentCells.length; i++) {
        if ((blankTile[0] == adjacentCells[i][0]) && (blankTile[1] == adjacentCells[i][1])) {
            swap(blankTile,clickedCell);
        }
    }
}

// this function swaps the clicked tile with the adjacent blank tile
function swap(blankTile,clickedCell){

    var parent_a = clickedCell.parentElement;
    var parent_b = cellSearch(blankTile,'elem');

    var a = document.getElementById(parent_a.firstChild.id);
    var b = document.getElementById(parent_b.firstChild.id);

    parent_b.replaceChild(a,b);

    var c = document.createElement('li');
    c.setAttribute('class', 'tile');
    c.setAttribute('id','9');
    parent_a.appendChild(c);
    parent_a.replaceChild(b,c);
}

// GLOBAL  =========================================
setID();
createTable();
console.log('To shuffle the table, type " shuffleTable(); " below and press enter');


document.addEventListener('click', function(e) {
    if (e.target.matches('.tile')) { //only registers clicks for tiles on the board
        var clicked_cell =  e.target;

        //searches tiles adjacent to
        var blankTile = cellSearch(clicked_cell,'blank');
        var adjacentCells = cellSearch(clicked_cell,'adj');
        checkSwap(blankTile,adjacentCells,clicked_cell);
    }
});
