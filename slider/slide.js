var elem = document.querySelector('.slider');


var flkty = new Flickity( elem, {
  // options
  // adaptiveHeight: true,
  cellAlign: 'center',
  contain: true,
  friction: 0.2,
  pageDots: false,
  prevNextButtons: false,

});



flkty.on('change', function(index) {

    //this funciton will run every time a card is changed, and will return the index current card in the viewport
    var card = index + 1;

    //selects the title for the appropraite card coming into view
    var title = document.getElementById('slider-' + card + '').querySelectorAll('.cardTitle');

    //animate the title to move horizontally
    Velocity(title, {
        scale:1.5,
        transform: "translateX(150)"
    },2000);

});









// Velocity(slider, { opacity: 0.1 }, { duration: 1000 });
